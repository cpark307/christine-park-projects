Java projects completed throughout CSC116 and CSC216

ServiceManager: A program that will help manage the queue for a car service garage
WolfScheduler:  A course registration program that would allow students to drop/add courses
                and admins to add/drop students/courses
Admissions:     A simple command line program that can determine if a student will be admitted
                based on GPA, SAT, essay score, and alumni factor
TabConverter:   Expands input file by creating an output file with the same content except 
                with all tab characters replaced with a given number of spaces





